// define $
$ = jQuery.noConflict();

// Width detector. Good for triggering responsive actions
$(document).ready(function(){
	var compareWidth; 
	var detector;
	detector = $('body'); // whatever container accurately reflects the site width
	compareWidth = detector.width();
	$(window).resize(function(){
		if(detector.width()!=compareWidth){
			
			if(detector.width()>1023){
			  $('.header-buttons').attr('style', '');
			  if($('.home').length) {
			    $('.hp-section').attr('style', '').removeClass('prev-section').removeClass('active');
          document.body.scrollTop = document.documentElement.scrollTop = 0;
          $('#section-1').addClass('active');
			  }
			}
			
			compareWidth = detector.width();
		}
	});
	
	$('header').on('click', '.header-button-toggle',  function(event) {
  	event.preventDefault()
  	$('.header-buttons').slideToggle();
	});
	
	$('.media-gallery ul').slick({
  	dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: false,
    arrows: false,
    swipeToSlide: true
	});
	
	if(detector.width() < 768 ){
  	$('.skills').slick({
    	dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      adaptiveHeight: false,
      arrows: false,
      swipeToSlide: true
  	});
	}
	
	$('.slick-slide').bind('touchstart', function(){ console.log('touchstart') });
	
	scrollfade_in_up($('.fade-in-out-scroll'), 100, 300, 2.5);
	
	
	//HP
	if($('.home').length && $('body').width() > 1023) {
	  get_page_height();
	}
	
	$(window).trigger('scroll');
	$(window).trigger('resize');
});

$(window).load(function(){
  $(window).trigger('scroll');
	$(window).trigger('resize');
	if($('.home').length) {
  	$('.hp-overlay').fadeOut(600, function(){
    	hp_load_animation();
  	});
	}
});

last_scroll_pos = $(window).scrollTop();

$(window).scroll(function(){
  if($('.home').length && $('body').width() > 1023) {
    scroll_section();
  }
});

$(window).resize(function(){
  if($('.home').length && $('body').width() > 1023) {
    
    get_page_height();
    
    $('.prev-section').each(function(){
      $(this).css('top', '-'+ $(this).outerHeight() +'px');
    });
    
    $('.hp-section').each(function(){
      previous_scrolled_data = 0;
      if(!$(this).hasClass('no-prev')){
        $(this).prevAll().each(function(){
          previous_scrolled_data = previous_scrolled_data + $(this).outerHeight();
        });
        $(this).attr('data-prev_scroll', previous_scrolled_data);
      }
    });
    
  }
});



if($('.archive').length) {
	
	var lFollowX = 0,
      lFollowY = 0,
      x = 0,
      y = 0,
      friction = 1 / 20;
  
  function moveBackground() {
    x += (lFollowX - x) * friction;
    y += (lFollowY - y) * friction;
    
    translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';
    translate_logo = 'translate(' + ((x / 4) * -1) + 'px, ' + ((y / 4) * -1) + 'px)';
  
    $('.work-item').mousemove(function(e) {
      $('.work-list-bg', this).css({
        'transition': 'none',
        '-webit-transform': translate,
        '-moz-transform': translate,
        'transform': translate
      });
      $('.work-list-logo', this).css({
        'transition': 'none',
        '-webit-transform': translate_logo,
        '-moz-transform': translate_logo,
        'transform': translate_logo
      });
    });
    
    $('.work-item').mouseout(function(e){
      lFollowX = 0;
      lFollowY = 0;
      x = 0;
      y = 0;
      x += (lFollowX - x) * friction;
      y += (lFollowY - y) * friction;
      
      translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';
      
      translate_logo = 'translate(' + x + 'px, ' + y + 'px)';
      
      $('.work-list-bg', this).css({
        'transition': 'all .4s ease-in-out',
        '-webit-transform': translate,
        '-moz-transform': translate,
        'transform': translate
      });
      
      $('.work-list-logo', this).css({
        'transition': 'all .4s ease-in-out',
        '-webit-transform': translate_logo,
        '-moz-transform': translate_logo,
        'transform': translate_logo
      });
    });
  
    window.requestAnimationFrame(moveBackground);
  }
  
  $('.work-item').mousemove(function(e) {
    var parentOffset = $(this).offset(); 
    var relX = e.pageX - parentOffset.left;
    var relY = e.pageY - parentOffset.top;
    var lMouseX = Math.max(-100, Math.min(100, $(this).width() / 2 - relX));
    var lMouseY = Math.max(-100, Math.min(100, $(this).height() / 2 - relY));
    lFollowX = (10 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
    lFollowY = (10 * lMouseY) / 100;
  });
  
  
  moveBackground();
}

function scrollfade_in_up(element, range, manualoffset, emthreshold){
    if($(window).height() < 450){
        manualoffset = manualoffset / 4;
    }
    $(window).scroll(function() {
        $(element).each(function(){
            var scrollTop = $(window).scrollTop(); 
            var offset = $(this).offset().top; 
            var height = $(this).outerHeight(); 
            var window_height = $(window).outerHeight(); 
            var window_offset_bottom = window_height - manualoffset; 
            var window_offset_top = window_height - manualoffset - range; 
            
            var calc = 1 + (scrollTop - offset + window_offset_bottom) / range;
            top_calc = emthreshold - calc;
            $(this).css({ 'opacity': calc });
            $(this).css({ 'top': top_calc + 'rem' });
            
            if ( calc > '1' ) {
                $(this).css({ 'opacity': 1 });
            } else if ( calc < '0' ) {
                $(this).css({ 'opacity': 0 });
            }
            if ( top_calc >= emthreshold ) {
                $(this).css({ 'top': emthreshold +'rem' });
            } else if ( top_calc < '0' ) {
                $(this).css({ 'top': 0 });
            }
        });
        
    }).scroll();
}

//homepage
function get_page_height(){
  var page_height = 0;
  $('.hp-section').each(function(){
    var section_height = $(this).outerHeight();
    page_height = page_height + section_height;
  });
  $('body').css('height', page_height);
}

function scroll_section(){
  var scrolled = $(window).scrollTop();
  var current_scrolled = scrolled - previous_scrolled;
  var active_section = '.hp-section.active';
  var this_section = active_section;
  var active_section_id = $(active_section).data('section');
  var active_section_height = section_height(active_section);
  var previous_scrolled = 0;
  var next_section = active_section_id + 1;
  var prev_section = active_section_id - 1;
  
  if(!$(active_section).hasClass('no-prev')){
    $('.prev-section').each(function(){
      previous_scrolled = previous_scrolled + $(this).outerHeight();
    });
    //$(active_section).attr('data-prev_scroll', previous_scrolled);
    //previous_scrolled = $(active_section).prevAll().outerHeight();
  } 
  
  
  var scroll_calc = scrolled - previous_scrolled;
  
  $(active_section).css('transform', 'translateY(-'+scroll_calc+'px)');
  //console.log('scrolled: '+scrolled);
  //console.log('active_section_height: '+active_section_height);
  if(scrolled >= active_section_height + previous_scrolled){
    $(active_section).removeClass('active').addClass('prev-section').attr('style', '').css('top','-'+active_section_height+'px');
    $('.hp-section[data-section='+next_section+']').addClass('active');
  }
  
  if(last_scroll_pos > scrolled){
    //going up
    active_section_prev_scrolled = $(active_section).data('prev_scroll');
    if(active_section_prev_scrolled >= scrolled){
      if(!$(active_section).hasClass('no-prev')){
        $(active_section).removeClass('active').attr('style', '');
        var initial_transform = $('.hp-section[data-section='+prev_section+']').outerHeight();
        $('.hp-section[data-section='+prev_section+']').addClass('active').removeClass('prev-section').attr('style', '').css('transform', 'translateY(-'+initial_transform+'px)');
      }
    }
  } else {
    //going down
  }
  last_scroll_pos = scrolled;
}

function section_height(section){
  return $(section).outerHeight();
}

function hp_load_animation(){
  var time = 100;

  $('.fade-in-hp').each(function() {  
    var item = $(this);  
    setTimeout( function(){ 
      item.addClass('loaded'); 
      //console.log(item); 
    }, time)
    time += 400;
  });
  
}

function opacity_class($item){
  $item.addClass('loaded')
}