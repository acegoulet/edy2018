// define $
$ = jQuery.noConflict();

function play_stop_video(){
  $('video.embed-video').each(function(){
    if($(this).visible(true, true)){
      if($(this).get(0).paused){
        var this_video = $(this);
        var play_promise = this_video.get(0).play();
        
        if (play_promise !== undefined) {
          play_promise.catch(error => {
            //console.log('no play');
            this_video.attr('controls', 'controls');
          }).then(() => {
            //console.log('play');
          });
        }
      }
    }
    else {
      if(!$(this).get(0).paused){
        $(this).get(0).pause();
      }
    }
  });
  
}

$(window).scroll(function(){
  play_stop_video();
});