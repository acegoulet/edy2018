<?php if(get_field('force_login', 'option')) { aceify_force_login(); } ?>
<!DOCTYPE html>
<html lang="en" class="gt-ie9" xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php wp_title(' &mdash; ',true,'right'); ?><?php bloginfo('name'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_directory'); ?>/img/apple-touch-icon.png?v=2">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory'); ?>/img/favicon-32x32.png?v=2">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory'); ?>/img/favicon-16x16.png?v=2">
	<link rel="manifest" href="<?php bloginfo('template_directory'); ?>/img/site.webmanifest">
	<link rel="mask-icon" href="<?php bloginfo('template_directory'); ?>/img/safari-pinned-tab.svg?v=2" color="#000000">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico?v=2">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?>">
	<meta name="application-name" content="<?php bloginfo('name'); ?>">
	<meta name="msapplication-config" content="<?php bloginfo('template_directory'); ?>/img/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<meta property="og:title" content="<?php bloginfo('name'); ?>" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo get_site_url(); ?>" />
  <meta property="og:image" content="<?php bloginfo('template_directory'); ?>/img/splash-page-og-image.jpg" />
  <meta property="og:image:width" content="1410" />
  <meta property="og:image:height" content="1000" />
  <meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
  <meta property="og:description" content="<?php the_field('headline', 61) ?>" />
  <meta name="twitter:card" content="summary_large_image" />
  <meta name="twitter:site" content="@edithlevin" />
  <meta name="twitter:domain" content="<?php echo get_site_url(); ?>" />
  <meta name="twitter:title" content="<?php bloginfo('name'); ?>" />
  <meta name="twitter:description" content="<?php the_field('headline', 61) ?>" />
  <meta name="twitter:image" content="<?php bloginfo('template_directory'); ?>/img/splash-page-og-image.jpg" />
  <meta itemprop="image" content="<?php bloginfo('template_directory'); ?>/img/splash-page-og-image.jpg" />
	<?php wp_head(); ?>
</head>

<?php 
  $header_body_class = '';
  if(!is_front_page()){
    $header_body_class = 'header-padding';
  }
?>
<body <?php body_class($header_body_class); ?>>
  <?php if(!is_front_page()){ ?>
  
    <header class="shadow">
      <?php 
        $header_back_link = get_site_url();
        $header_back_text = get_bloginfo('name');
        $header_title = get_the_title();
        if(is_singular('work')){
          $header_back_link = get_post_type_archive_link('work');
          $header_back_text = 'All Work';
        }
        if(is_post_type_archive('work')){
          $header_title = 'All Work';
        }
      ?>
      <div class="container">
        <a class="header-back" href="<?php echo $header_back_link; ?>"><span class="mobile-hide"><?php echo $header_back_text; ?></span></a>
        <h1 class="header-title text-center"><?php echo $header_title; ?></h1>
        <?php if(is_single() && get_field('header_buttons')){ ?>
          <a href="javascript:void(0)" class="header-button-toggle"></a>
          <div class="header-buttons">
            <?php foreach(get_field('header_buttons') as $button){ ?>
              <a href="<?php echo $button['link']; ?>" target="_blank" class="button button-outline button-sm"><?php echo $button['title']; ?></a>
            <?php } ?>
          </div>
        <?php } ?>
      </div>
    </header>
  
  <?php } ?>