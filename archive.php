<?php get_header(); ?>
  <?php query_posts('showposts=-1&post_type=work&orderby=menu_order&order=ASC'); ?>
  <div class="container padding-vert-70">
  	<?php while (have_posts()) : the_post(); ?>
  		  
  		<a href="<?php the_permalink(); ?>" class="work-item bg-black grid_5 push_1" >
    		<div class="work-list-bg bg-cover" style="background-image: url(<?php echo get_image_url(get_field('work_archive_image'), 'work-archive'); ?>);"></div>
    		<div class="work-list-overlay"></div>
    		<?php if(!get_field('work_archive_logo')){ ?>
  			  <div class="work-list-title text-center"><?php the_title(); ?></div>
  			<?php } else { ?>
  			  <div class="work-list-logo bg-100" style="background-image: url(<?php echo get_image_url(get_field('work_archive_logo'), 'full'); ?>);"></div>
  			<?php } ?>
  			<?php if(get_field('tags')){ ?>
    			<div class="tags">
      			<div class="tag-divider"></div>
      			<ul>
        			<?php 
          			foreach(get_field('tags') as $tag){
            			echo '<li>'.$tag['tag'].'</li>';
          			}
        			?>
      			</ul>
    			</div>
  			<?php } ?>
  			<div class="work-list-clear-overlay"></div>
  		</a>
  		
  	<?php endwhile; ?>
  	<div class="clear"></div>
  </div>
<?php get_footer(); ?>