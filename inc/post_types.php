<?php

if ( ! function_exists( 'aceify_reg_post_types' ) ) {

    add_action('init', 'aceify_reg_post_types');
    
    function aceify_reg_post_types(){
        $labels = array(
    		'name' => _x('Work', 'post type general name'),
    		'singular_name' => _x('Work Item', 'post type singular name'),
    		'add_new' => _x('Add New Work Item', 'bdes'),
    		'add_new_item' => __('Add New Work Item'),
    		'edit_item' => __('Edit Work Item'),
    		'new_item' => __('New Work Item'),
    		'view_item' => __('View Work Item'),
    		'search_items' => __('Search Work'),
    		'not_found' =>  __('Nothing found'),
    		'not_found_in_trash' => __('Nothing found in Trash'),
    		'parent_item_colon' => ''
    	);
         
    	$args = array(
    		'labels' => $labels,
    		'label'	=> 'Work',
    		'public' => true,
    		'publicly_queryable' => true,
    		'description' => 'Work Item',
    		'show_ui' => true,
    		'query_var' => true,
    		'capability_type' => 'post',
    		'hierarchical' => true,
    		'has_archive' => true,
        'dashicon' => 'dashicons-art',
    		'supports' => array('title','editor','revisions','page-attributes'),
    		'taxonomies' => array()
        ); 
     
        register_post_type( 'work' , $args );          
    }
    
}

?>