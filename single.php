<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>

<?php if( !post_password_required() ){ ?>

	<article <?php post_class(); ?>>
  	<?php if(get_field('header_image')){ ?>
		  <div class="hero bg-cover" style="background-image: url(<?php echo get_image_url(get_field('header_image'), 'work-header'); ?>);"></div>
		<?php } ?>
		
		<?php if(get_the_content()) { ?>
  		<div class="container padding-vert-60">
    		<div class="grid_8 push_2 padding-horizontal-15 entry-content">
      		<?php the_content(); ?>
    		</div>
    		<div class="clear"></div>
  		</div>
		<?php } ?>
		
		<?php if(get_field('black_row_text_column_1') || get_field('black_row_text_column_2')){ ?>
  		<div class="bg-black">
    		<div class="container padding-vert-60">
      		<div class="grid_5 push_1 padding-horizontal-15 entry-content mobile-bottom-margin fade-in-out-scroll">
        		<?php the_field('black_row_text_column_1'); ?>
      		</div>
      		<div class="grid_5 push_1 padding-horizontal-15 entry-content fade-in-out-scroll">
        		<?php the_field('black_row_text_column_2'); ?>
      		</div>
      		<div class="clear"></div>
    		</div>
  		</div>
		<?php } ?>
		
		<?php 
  		$main_content = get_field('main_content');
    ?>
    <?php if(!empty($main_content)){ ?>
      <?php 
        $main_content_bg = 'bg-white';
        if(get_field('main_content_background_color') == 'grey'){
          $main_content_bg = 'bg-light-grey';
        }
        $width_counter = 0;
      ?>
      <div class="additional-content container padding-top-60 <?php echo $main_content_bg; ?>">
        <?php foreach($main_content as $content_item){ ?>
        
          <?php 
            //print_r($content_item);
            $layout = $content_item['acf_fc_layout'];
            $width = 'full';
            $width_class = 'grid_10 push_1';
            $width_count = 2;
            if(isset($content_item['width'])){
              $width = $content_item['width'];
            }
            if($width == 'half'){
              $width_class = 'grid_5 push_1';
              $width_count = 1;
            }
            if($width == 'mobile'){
              $width_class = 'mobile-module grid_4 push_1';
              $width_count = 1;
            }
            $background_class = '';
            $additional_space_classes = '';
            if(isset($content_item['background_color']) && $content_item['background_color'] !== 'none'){
              $additional_space_classes = 'padding-top-60 margin-bottom-60';
              if($content_item['background_color'] == 'white'){
                $background_class = 'bg-white padding-horizontal-9-4';
              }
              else if($content_item['background_color'] == 'grey'){
                $background_class = 'bg-light-grey padding-horizontal-9-4';
              }
            }
          ?>
          <div class="content-module fade-in-out-scroll <?php echo $background_class; ?> <?php echo $width_class; ?> <?php echo $additional_space_classes; ?> padding-bottom-60">
            
            <?php if($layout == 'copy'){ ?>
              <div class="entry-content padding-horizontal-15">
      		      <?php echo $content_item['copy']; ?>
    		      </div>
            <?php } ?>
            
            <?php if($layout == 'media'){ ?>
              <?php 
      		      $media_type = $content_item['media_type'];
      		      $media_image = $content_item['image'];
      		      $media_vimeo = $content_item['vimeo_id'];
      		      $media_video_embed = $content_item['video_embed'];
      		      $media_gallery = $content_item['gallery'];
      		      $media_size = $width;
      		      include(locate_template('templates/partials/media.php'));
      		    ?>
            <?php } ?>
            
            <?php if($layout == 'title'){ ?>
              <h2 class="text-center"><?php echo $content_item['title']; ?></h2>
            <?php } ?>
            
            <?php if($layout == 'callout_text'){ ?>
              <div class="callout text-center padding-horizontal-9-4">
                <?php echo $content_item['callout_text']; ?>
              </div>
            <?php } ?>
            
          </div>
          <?php 
            $width_counter += $width_count;
            if ($width_counter >= 2) {
              $width_counter = 0;
              echo '<div class="clear"></div>';
            }
          ?>
        <?php } ?>
      </div>
    <?php } ?>
		
		<?php 
  		$additional_content = get_field('additional_content');
		?>
		<?php if(!empty($additional_content)){ ?>
		  <div class="additional-content container">
  		  <?php if($additional_content['additional_content_title']){ ?>
  		    <div class="padding-vert-60 text-center grid_8 push_2"><h2 class="fade-in-out-scroll"><?php echo $additional_content['additional_content_title']; ?></h2></div>
  		    <div class="clear"></div>
  		  <?php } ?>
  		  <?php 
    		  $additional_counter = 0;
    		  if (!empty($additional_content['content'])) foreach($additional_content['content'] as $additional_content_item){
      		  $text_align = 'push_1';
      		  if($additional_counter % 2 == 0){
        		  $text_align = 'right-align pull_1';
      		  }
        ?>
  		      <div class="content-row margin-bottom-60">
    		      <div class="grid_5 fade-in-out-scroll <?php echo $text_align; ?> entry-content content-copy">
      		      <?php echo $additional_content_item['copy']; ?>
    		      </div>
    		      
    		      <div class="grid_5 push_1 media-wrapper fade-in-out-scroll">
      		      <?php 
        		      $media_type = $additional_content_item['media_type'];
        		      $media_image = $additional_content_item['image'];
        		      $media_vimeo = $additional_content_item['vimeo_id'];
        		      $media_video_embed = $additional_content_item['video_embed'];
        		      $media_gallery = $additional_content_item['gallery'];
        		      $media_size = 'half';
        		      include(locate_template('templates/partials/media.php'));
        		    ?>
    		      </div>
    		      
    		      <div class="clear"></div>
  		      </div>
  		  <?php 
    		    $additional_counter++;
    		  } 
    		?>
		  </div>
		<?php } ?>
		
		<?php if(get_field('additional_footer_copy')){ ?>
  		<div class="addition-footer-wrapper">
  		  <div class="container margin-bottom-60">
  		    <div class="addition-footer-copy bg-light-grey grid_10 push_1 padding-vert-60 padding-horizontal-9-4 entry-content fade-in-out-scroll">
    		    <?php the_field('additional_footer_copy'); ?>
  		    </div>
  		    <div class="clear"></div>
  		  </div>
  		</div>
		<?php } ?>
		
		<?php if(get_field('credits_title') || get_field('credit_groups')){ ?>
  		<div class="credit-wrapper padding-vert-60 bg-light-grey text-center">
  		  <div class="container">
    		  <div class="grid_8 push_2 margin-bottom-30 fade-in-out-scroll"><span class="section-title"><?php the_field('credits_title'); ?></span></div>
  		    <div class="grid_8 push_2 credit-group-wrapper fade-in-out-scroll">
    		    <?php foreach(get_field('credit_groups') as $credit_group){ ?>
    		      <div class="credit-group">
      		      <?php foreach($credit_group['credit_group'] as $credit){ ?>
      		        <h3 class="credit"><em class="med-dark-grey"><?php echo $credit['light_text']; ?></em><?php echo $credit['dark_text']; ?></h3>
      		      <?php } ?>
    		      </div>
    		    <?php } ?>
  		    </div>
  		    <div class="clear"></div>
  		  </div>
  		</div>
		<?php } ?>
	</article>
	
<?php } else { echo get_the_password_form(); } ?>
	
<?php endwhile; ?>

<?php get_footer(); ?>