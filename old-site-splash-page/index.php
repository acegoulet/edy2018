<?php 
  //error_reporting(E_ALL);
  //ini_set('display_errors', '1');
  $json = file_get_contents('https://edithgoulet.com/old-site-api/');
  $content = json_decode($json);
?>
<!DOCTYPE html>
<html lang="en" class="gt-ie9" xmlns="http://www.w3.org/1999/xhtml">

  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  	<title><?php echo $content->meta_title; ?></title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />
  	<link rel="apple-touch-icon" sizes="180x180" href="//edithgoulet.com/wp-content/themes/edith-2018/img/apple-touch-icon.png?v=3">
  	<link rel="icon" type="image/png" sizes="32x32" href="//edithgoulet.com/wp-content/themes/edith-2018/favicon-32x32.png?v=3">
  	<link rel="icon" type="image/png" sizes="16x16" href="//edithgoulet.com/wp-content/themes/edith-2018/img/favicon-16x16.png?v=3">
  	<link rel="manifest" href="//edithgoulet.com/wp-content/themes/edith-2018/img/manifest.json">
  	<link rel="mask-icon" href="//edithgoulet.com/wp-content/themes/edith-2018/img/safari-pinned-tab.svg?v=3" color="#000000">
  	<link rel="shortcut icon" href="//edithgoulet.com/wp-content/themes/edith-2018/img/favicon.ico?v=3">
  	<meta name="apple-mobile-web-app-title" content="<?php echo $content->meta_title; ?>">
  	<meta name="application-name" content="<?php echo $content->meta_title; ?>">
  	<meta name="msapplication-config" content="//edithgoulet.com/wp-content/themes/edith-2018/img/browserconfig.xml">
  	<meta name="theme-color" content="#ffffff">
  	
    <meta name="description"  content="<?php echo $content->meta_description; ?>" />
    
    <link rel="canonical" href="http://www.edithlevin.com/" />
    <meta property="og:title" content="<?php echo $content->meta_title; ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.edithlevin.com/" />
    <meta property="og:image" content="<?php echo $content->social_share_image; ?>" />
    <meta property="og:site_name" content="<?php echo $content->meta_title; ?>" />
    <meta property="og:description" content="<?php echo $content->meta_description; ?>" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@edithlevin" />
    <meta name="twitter:domain" content="edithlevin.com" />
    <meta name="twitter:title" content="<?php echo $content->meta_title; ?>" />
    <meta name="twitter:description" content="<?php echo $content->meta_description; ?>" />
    <meta name="twitter:image" content="<?php echo $content->social_share_image; ?>" />
    <meta itemprop="image" content="<?php echo $content->social_share_image; ?>" />
    <link rel='stylesheet' href='old-site.css' type='text/css' media='all' />
    
    <style>
      .headline-image {
        background-image: url(<?php echo $content->background_image; ?>);
      }
    </style>
  </head>
  
  <body>
    <div class="hero bg-black">
      <div class="headline-image"></div>
      <div class="container">
        <div class="grid_8 push_1">
          <h1 class="headline"><?php echo $content->headline; ?></h1>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <footer class="bg-accent">
      <div class="container">
        <h2 class="footer-title text-center"><?php echo $content->contact_title; ?></h2>
        
        <div class="footer-buttons">
          <?php 
            if(!empty($content->buttons)){
              foreach($content->buttons as $button){
                echo '<div class="grid_4 padding-sides"><a class="button button-white" href="'.$button->button_destination.'" target="_blank"><span class="button-icon" style="background-image: url('.$button->button_icon.');"></span><span class="button-text">'.$button->button_text.'</span></a></div>';
              }
            }
          ?>
          <div class="clear"></div>
        </div>
        
        <div class="grid_12 footer-meta">
          <span class="footer-copyright float-left margin-right-20"><?php echo $content->copyright; ?></span>
          <span class="footer-photo-credit float-left"><?php echo $content->photo_credit; ?></span>
          <span class="footer-dev-credit float-right"><?php echo $content->dev_credit; ?></span>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
    </footer>
  </body>

</html>