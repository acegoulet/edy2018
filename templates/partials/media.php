<?php
  if($media_type == 'image'){
    if(!empty($media_image)){
      echo '<div class="media-image"><img src="'.get_image_url($media_image, 'work-'.$media_size).'" alt="" /></div>';
    }
  }
  else if($media_type == 'vimeo'){ 
    if(!empty($media_vimeo)){
?>
      <div class="media-vimeo vimeo-wrapper">
        <iframe src="https://player.vimeo.com/video/<?php echo $media_vimeo; ?>?title=0&portrait=0&byline=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
      </div>
<?php   
    }
  }
  else if($media_type == 'video_embed'){ 
    if(!empty($media_video_embed)){ 
?>
      <div class="media-video-embed">
        <video class="embed-video" preload loop>
          <source src="<?php echo $media_video_embed['url']; ?>" type="video/mp4">
          Your browser does not support the video tag.
        </video>
        <?php if(!empty($media_image)){ ?>
          <div class="mobile-fallback-image">
            <img src="<?php echo get_image_url($media_image, 'work-'.$media_size); ?>" alt="" />
          </div>
        <?php } ?>
      </div>
<?php   
    }
  }
  else if($media_type == 'gallery'){ 
    if(!empty($media_gallery)){
?>
      <div class="media-gallery">
        <ul>
          <?php foreach($media_gallery as $slide){ 
            echo '<li>';
              if($slide['type'] == 'image') {
            ?>
              <img src="<?php echo get_image_url($slide['image'], 'work-'.$media_size); ?>" />
            <?php 
              }
              else if($slide['type'] == 'vimeo'){
            ?>
                <div class="media-vimeo vimeo-wrapper">
                  <iframe src="https://player.vimeo.com/video/<?php echo $slide['vimeo_id']; ?>?title=0&portrait=0&byline=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            <?php    
              }
            echo '</li>';
          }
          ?>
        </ul>
      </div>
<?php   
    }
  }
?>