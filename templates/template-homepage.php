<?php /* Template Name: Homepage */ ?>
<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

<div class="hp-section-wrapper">
	<div class="hp-section active no-prev" id="section-1" data-section="1">
		<section id="hero" class="home-hero hero bg-black">
  		<div class="container headline-wrapper">
        <div class="grid_8 push_1 padding-vert-2">
          <h1 class="headline fade-in-hp opacity-transition"><?php the_field('headline') ?></h1>
        </div>
        <div class="clear"></div>
      </div>
        <?php if(get_field('work_items')){ ?>
        
          <?php 
            $work_count = count(get_field('work_items'));
            $work_count_half = $work_count / 2; 
            $work_counter = 0;
            
            foreach(get_field('work_items') as $work_item){
              if($work_counter == 0){
                echo '<div class="home-work-top-row home-work-row">';
              }
              
              ?>
                
                <a href="<?php the_permalink($work_item) ?>" class="home-work-item fade-in-hp">
                  <div class="home-work-item-inner bg-cover" style="background-image: url(<?php echo get_image_url(get_field('homepage_image', $work_item->ID), 'home-work'); ?>)">
                    <?php 
                      $bw_logo = get_image_url(get_field('bw_logo', $work_item->ID), 'full');
                      $color_logo = $bw_logo;
                      if(get_field('color_logo_copy', $work_item->ID)){
                        $color_logo = get_image_url(get_field('color_logo_copy', $work_item->ID), 'full');
                      }
                    ?>
                    <div class="work-logo logo-bw bg-100" style="background-image: url(<?php echo $bw_logo; ?>)"></div>
                    <div class="work-logo logo-color bg-100" style="background-image: url(<?php echo $color_logo; ?>)"></div>
                  </div>
                </a>
                
              <?php
              
              $work_counter++;
              
              if($work_counter == $work_count_half){
                  echo '<div class="clear"></div>';
                echo '</div>';
                echo '<div class="home-work-bottom-row home-work-row">';
              }
              
              if($work_counter == $work_count){
                  echo '<div class="clear"></div>';
                echo '</div>';
              }
            }
          ?>
          
        <?php } ?>
		</section>
	</div>
	<div class="hp-section" id="section-2" data-section="2">
		<section id="skills" class="subsequent bg-white">
  		<div class="container">
    		<div class="grid_8 push_2 text-center padding-vert-50">
      		<div class="padding-sides">
      		  <?php the_field('work_copy'); ?>
      		</div>
      		<a href="<?php echo get_post_type_archive_link('work'); ?>" class="button margin-top-20 filled-button"><?php the_field('work_button_text'); ?></a>
    		</div>
    		<div class="clear"></div>
  		</div>
  		<div class="bg-light-grey padding-vert-40 padding-bottom-70">
    		<div class="container skills">
      		<?php 
        		$skills = get_field('skills');
        		if(!empty($skills)){
          		foreach($skills as $skill){
          ?>
                <div class="grid_4 skill bg-white shadow">
                  <div class="skill_pattern" style="background-image:url(<?php echo get_image_url($skill['pattern'], 'full'); ?>);"></div>
                  <div class="skill-copy">
                    <h2 class="text-center"><?php echo $skill['title']; ?></h2>
                    <?php echo $skill['copy']; ?>
                  </div>
                </div>
          <?php
          		}
        		}
      		?>
      		
    		</div>
  		</div>
		</section>
  </div>
	<div class="hp-section" id="section-3" data-section="3">
		<section id="about" class="subsequent padding-bottom-50 bg-white">
  		<div class="about-image bg-cover margin-bottom-50" style="background-image: url(<?php echo get_image_url(get_field('about_image'), 'about'); ?>);"></div>
  		<div class="container">
  		  <div class="grid_6 push_1 padding-horizontal-15">
    		  <?php the_field('bio'); ?>
  		  </div>
  		  <div class="grid_4 push_1 padding-horizontal-15 padding-top-20">
    		  <h3><?php the_field('tools_title'); ?></h3>
    		  <div class="list-wrapper">
      		  <?php foreach(get_field('tools_logos') as $logo){ ?>
      		    <img class="list-logo" alt="<?php echo $logo['title']; ?>" src="<?php echo $logo['sizes']['tool-logo']; ?>" />
      		  <?php } ?>
    		  </div>
    		  <h3><?php the_field('supporting_arsenal_title'); ?></h3>
    		  <div class="list-wrapper">
      		  <?php foreach(get_field('supporting_logos') as $logo){ ?>
      		    <img class="list-logo" alt="<?php echo $logo['title']; ?>" src="<?php echo $logo['sizes']['tool-logo']; ?>" />
      		  <?php } ?>
    		  </div>
    		  <h3><?php the_field('smitten_list_title'); ?></h3>
    		  <p class="list-wrapper"><?php the_field('smitten_list'); ?></p>
  		  </div>
    		<div class="clear"></div>
  		</div>
		</section>
  </div>
	<div class="hp-section no-next" id="section-4" data-section="4">
		<section id="resume" class="subsequent padding-vert-60 bg-light-grey resume-section">
  		<div class="container">
    		<div class="grid_6 push_2">
      		<span class="section-title"><?php the_field('resume_title'); ?></span>
        </div>
    		<div class="grid_2 push_2">
      		<?php if(get_field('resume_pdf')){ ?>
      		  <a href="<?php the_field('resume_pdf'); ?>" target="_blank" class="button filled-button button-sm margin-top-20">Download PDF</a>
          <?php } ?>
    		</div>
    		<div class="clear"></div>
    		<div class="mobile-hide margin-top-40">
      		
      		<?php 
        		$resume_sections = get_field('resume_sections');
        		
        		foreach($resume_sections as $resume_section){
          ?>
              <div class="grid_2 push_2">
                <h2><?php echo $resume_section['section_title']; ?></h2>
              </div>
              <div class="grid_6 push_2 padding-horizontal-15 margin-bottom-40">
                <?php 
                  $section_items = $resume_section['section_items'];
                  foreach($section_items as $section_item){
                ?>
                    <div class="resume-item">
                      <h3><?php echo $section_item['name']; ?>, <span><?php echo $section_item['title']; ?></span></h3>
                      <h4><?php echo $section_item['date']; ?></h4>
                      <?php echo $section_item['copy']; ?>
                    </div>
                <?php
                  }
                  
                ?>
              </div>
              <div class="clear"></div>
          <?php
        		}
        		
      		?>
      		
    		</div>
  		</div>
		</section>
	
	
<?php endwhile; ?>

<?php get_footer(); ?>