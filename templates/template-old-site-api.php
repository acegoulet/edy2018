<?php /* Template Name: Old Site API */

$data = array();

while (have_posts()) : the_post();

  $data['headline'] = get_field('headline');
  $data['background_image'] = get_field('background_image');
  $data['contact_title'] = get_field('contact_title');
  $data['copyright'] = get_field('copyright');
  $data['photo_credit'] = get_field('photo_credit');
  $data['dev_credit'] = get_field('dev_credit');
  $data['meta_title'] = get_field('meta_title');
  $data['meta_description'] = get_field('meta_description');
  $data['social_share_image'] = get_field('social_share_image');
  $data['buttons'] = array();
  if(get_field('buttons')){
    $button_count = 0;
    foreach( get_field('buttons') as $button) {
      $data['buttons'][$button_count]['button_text'] = $button['button_text'];
      $data['buttons'][$button_count]['button_destination'] = $button['button_destination'];
      $data['buttons'][$button_count]['button_icon'] = $button['button_icon'];
      $button_count++;
    }
  }
	
endwhile;

header("Content-type: application/json; charset=utf-8");
die(json_encode($data));

?>