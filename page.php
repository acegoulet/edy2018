<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>

<?php if( !post_password_required() ){ ?>

	<article <?php post_class(); ?>>
  	<?php if(get_field('header_image')){ ?>
		  <div class="hero bg-cover" style="background-image: url(<?php echo get_image_url(get_field('header_image'), 'work-header'); ?>);"></div>
		<?php } ?>
		
		<?php if(get_the_content()) { ?>
  		<div class="container padding-vert-60">
    		<div class="grid_8 push_2 padding-horizontal-15 entry-content">
      		<?php the_content(); ?>
    		</div>
    		<div class="clear"></div>
  		</div>
		<?php } ?>
		
<?php } else { echo get_the_password_form(); } ?>
	
<?php endwhile; ?>

<?php get_footer(); ?>