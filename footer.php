  <footer class="bg-accent">
    <div class="container">
      <h2 class="footer-title text-center"><?php the_field('footer_title', 'options'); ?></h2>
      
      <div class="footer-buttons">
        <?php 
          if(!empty(get_field('buttons', 'options'))){
            foreach(get_field('buttons', 'options') as $button){
              echo '<div class="grid_4 padding-sides"><a class="button button-white" href="'.$button['button_destination'].'" target="_blank"><span class="button-icon" style="background-image: url('.$button['button_icon'].');"></span><span class="button-text">'.$button['button_text'].'</span></a></div>';
            }
          }
        ?>
        <div class="clear"></div>
      </div>
      
      <div class="grid_12 footer-meta">
        <span class="footer-copyright float-left margin-right-20"><?php the_field('copyright', 'options'); ?></span>
        <span class="footer-photo-credit float-left"><?php the_field('photo_credit', 'options'); ?></span>
        <span class="footer-dev-credit float-right"><?php the_field('dev_credit', 'options'); ?></span>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </footer>
  <?php if(is_front_page()){ ?></div></div><div class="hp-overlay bg-black"><div class="loader"></div></div><?php } ?>
	<?php 
        wp_footer(); 
    ?>	
</body>
	
</html>
